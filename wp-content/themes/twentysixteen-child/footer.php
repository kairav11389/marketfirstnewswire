<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>

		</div><!-- .site-content -->
<?php echo do_shortcode('[newsletter_form]'); ?>
		<footer id="colophon" class="site-footer" role="contentinfo">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<?php if ( has_nav_menu( 'primary' ) ) : ?>
							<nav class="main-navigation" role="navigation" aria-label="<?php esc_attr_e( 'Footer Primary Menu', 'twentysixteen' ); ?>">
								<?php
									wp_nav_menu( array(
										'theme_location' => 'primary',
										'menu_class'     => 'primary-menu',
									 ) );
								?>
							</nav> <!-- .main-navigation -->
						<?php endif; ?>
					</div>
					<div class="col-12">
					</div>
				</div>
			</div>	

			<div class="site-info">
				<div class="container">
					<div class="row">
						<div class="col-6"><p>2018 © MarketFirstNewswire</p></div>
						<div class="col-6"><p>2018 © MarketFirstNewswire</p></div>
					</div>	
				</div>
			</div><!-- .site-info -->
		</footer><!-- .site-footer -->
	</div><!-- .site-inner -->
</div><!-- .site -->

<?php wp_footer(); ?>
</body>
</html>
