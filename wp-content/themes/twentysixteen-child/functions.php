<?php

add_action( 'wp_enqueue_scripts', 'enqueue_parent_styles' );
function enqueue_parent_styles() {
    /* wp_enqueue_style( 'parent-style', get_template_directory_uri().'/style.css' );
    wp_enqueue_style( 'child-style', get_stylesheet_directory_uri().'/style.css', array('parent-style')); */
	wp_enqueue_style( 'custom-child-navigation-style', get_stylesheet_directory_uri().'/custom.css');
	wp_enqueue_style( 'new-custom', get_stylesheet_directory_uri().'/new-custom.css', array('custom-child-navigation-style'));
	wp_enqueue_style( 'less-style', get_stylesheet_directory_uri().'/less/main.css', array('new-custom'));
}

function custom_footer_script() {
	include('custom.php');
}
add_action( 'wp_footer', 'custom_footer_script' );

// remove wp version param from any enqueued scripts
/* function vc_remove_wp_ver_css_js( $src ) {
    if ( strpos( $src, 'ver=' . get_bloginfo( 'version' ) ) )
        $src = remove_query_arg( 'ver', $src );
    return $src;
}
add_filter( 'style_loader_src', 'vc_remove_wp_ver_css_js', 9999 );
add_filter( 'script_loader_src', 'vc_remove_wp_ver_css_js', 9999 ); */

/* function wp_bootstrap_4_widgets_init_child() {
	register_sidebar( array(
		'name'          => esc_html__( 'Logo left content', 'wp-bootstrap-4' ),
		'id'            => 'logo-left-content',
		'description'   => esc_html__( 'Add widgets here.', 'wp-bootstrap-4' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h5 class="widget-title h6">',
		'after_title'   => '</h5>',
	) );
}
add_action( 'widgets_init', 'wp_bootstrap_4_widgets_init_child' ); */ 