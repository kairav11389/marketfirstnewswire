<?php

// Get IP address
$ip_address = getenv('HTTP_CLIENT_IP') ?: getenv('HTTP_X_FORWARDED_FOR') ?: getenv('HTTP_X_FORWARDED') ?: getenv('HTTP_FORWARDED_FOR') ?: getenv('HTTP_FORWARDED') ?: getenv('REMOTE_ADDR');

// Get JSON object
$jsondata = file_get_contents("http://timezoneapi.io/api/ip/?" . $ip_address);

// Decode
$data = json_decode($jsondata, true);


// Request OK?
if($data['meta']['code'] == '200'){
    // Example: Get the users time

	echo "<span class='fix-width'>Date Modified</span>" . $data['data']['datetime']['day'] . "/" . $data['data']['datetime']['month'] . "/" . $data['data']['datetime']['year'] . " &nbsp;" . $data['data']['datetime']['time'];	
}