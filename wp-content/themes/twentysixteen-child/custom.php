<script>
	jQuery(window).load(function() {
		jQuery(".loader-img").fadeOut();
	});
	
	jQuery(document).ready(function() {
		jQuery(".wppb-description-delimiter").each(function() {
			var getDescription = jQuery(this).text();
			jQuery(this).prev("input").attr('placeholder',getDescription);
		});
		
		var loginUser = jQuery("#loginform .login-username label").text();
		jQuery("#loginform .login-username input").attr('placeholder',loginUser);
		
		var loginPassword = jQuery("#loginform .login-password label").text();
		jQuery("#loginform .login-password input").attr('placeholder',loginPassword);
	});
</script>

<script>
jQuery(document).ready(function() {
	
	jQuery(".empty-date select option[value='']").text('Pick a Date');
	jQuery(".empty-hours .same-line:first-child select option[value='']").text('HH');
	jQuery(".empty-hours .same-line:last-child select option[value='']").text('MM');
	jQuery(".empty-first select option[value='']").text('Please Select a Headline');
	jQuery(".empty-company select option[value='']").text('Please Pick A Company');
	jQuery(".empty-specification select option[value='']").text('Please Pick A Classification');
	jQuery("input[name='dynamictext-633']").prop("readonly", true);
	jQuery( ".logged-in ul#top-menu > li:last-child" ).load( "/login-signup/ .user-login a" ).attr('href');
	
	jQuery("button.create-release").click(function() {
		//var data = jQuery("#form-time").serialize();
		var url = "<?php echo get_stylesheet_directory_uri(); ?>/inserttime.php";
		<?php $current_user = wp_get_current_user(); $user_id = get_current_user_id(); 
		$user_info = get_userdata($user_id);
		$user_firstname = trim(ucwords($user_info->user_firstname));
		$user_lastname = trim(ucwords($user_info->user_lastname));?>
		var user_id = "<?php echo $user_id;?>";
		var user_firstname = "<?php echo $user_firstname;?>";
		var user_lastname = "<?php echo $user_lastname;?>";
		
		jQuery.ajax({
			 data : {'user_id':user_id, 'user_firstname':user_firstname, 'user_lastname':user_lastname},
			 type: "post",
			 url: url,
			 success: function(data){
				jQuery('#custom-details	').html(data);
				jQuery("button.create-release").hide();
				jQuery(".form-to-create-release, .go-back, div#custom-details").show();	
			 }
		});
	});
	
	jQuery(".go-back-button").click(function() {
		jQuery(".form-to-create-release, .go-back, div#custom-details").hide();		
		jQuery("button.create-release").show();		
	});
	
	var val = jQuery(".empty-date select option[value='']").text();
	var url = '<?php echo get_stylesheet_directory_uri(); ?>/ajax-custom-select-option-for-select-date.php';
	jQuery.ajax({
	url: url,  //Pass URL here 
	type: "GET", //Also use GET method
	data: {'custom_option':val},
	success: function(check) {
		console.log("success:" + check);
		jQuery('.empty-date select').append(check);
		}
	});	
	
	jQuery(".select-second-option").click(function() {
		jQuery('.empty-date select option:nth-child(2)').attr("selected","selected");
	});
	
	jQuery(".radio-645 .wpcf7-radio span:nth-child(2) input, .radio-645 .wpcf7-radio span:nth-child(1) input, .radio-39 .wpcf7-radio span:nth-child(1) input, .radio-39 .wpcf7-radio span:nth-child(2) input, .radio-866 .wpcf7-radio span:nth-child(2) input, .radio-866 .wpcf7-radio span:nth-child(1) input, .radio-975 .wpcf7-radio span:nth-child(2) input, .radio-975 .wpcf7-radio span:nth-child(1) input").change(function(){ 	 
		var url = "<?php echo get_stylesheet_directory_uri(); ?>/ajax-update-dynamic-time.php";		
		jQuery.ajax({
			 type: "post",
			 url: url,
			 success: function(data){
				jQuery('#custom-details .left-content-account .dynamic-time').html(data);
			 }
		});
	});	
	
	jQuery(".empty-first select").change(function() {
		var option = jQuery(this).find(":selected").val();
	jQuery(".paste-same-text input").val(option);
	});
	
	// jQuery( ".empty-company select" ).each(function() {
		// alert(jQuery('.empty-company select option').val().toLowerCase());
	// });
		 
	jQuery('.select').click(function() {
		jQuery('.empty-company select option').each(function(){
			var text_reindex = jQuery(this).val(jQuery(this).text().toLowerCase());			
		});
		var prepop = jQuery('.empty-company input[type="text"]').val();
		//alert(prepop);
		jQuery('.empty-company select').find('option[value*="' + prepop + '"]').attr("selected","selected");
		
		jQuery('.empty-company select option').each(function(){
			var text_reindex = jQuery(this).val(jQuery(this).text());			
		});
	});
});
</script>	